
public class Program{
	public static void main(String[] args) {
		/*
	String s = "Java-Programm";
	int i = 123;
	
// Standardausgabe
System.out.printf( "\n|%s|\n", s );
// rechtsbündig mit 20 Stellen
System.out.printf( "|%20s|\n", s );
// linksbündig mit 20 Stellen
System.out.printf( "|%-20s|\n", s );
// minimal 5 stellen
System.out.printf( "|%5s|\n", s );
// maximal4 Stellen
System.out.printf( "|%.4s|\n", s );
// 20 Positionen, rechtsbündig, höchstens 4 Stellen von String
System.out.printf( "|%20.4s|\n", s );
// rechtsbündig (Stellen nach Bedarf)
System.out.printf( "|%d||%d|\n" ,i, -i);
// rechtsbündig (mindestens 5 Stellen)
System.out.printf( "|%5d| |%5d|\n" ,i, -i);
// linksbündig (mindestens 5 Stellen)
System.out.printf( "|%-5d| |%-5d|\n" ,i, -i);
// zusätzlich mit Vorzeichen (auch positive Zahlen)
System.out.printf( "|%+-5d| |%+-5d|\n" , i, -i);
// Auffüllen nach vorne mit Nullen
System.out.printf( "|%05d| |%05d|\n\n",i, -i);
// hexadezimale Darstellung in Groß-bzw. Kleinbuchstaben
System.out.printf( "|%X| |%x|\n", 0xabc, 0xabc );
// hexadezimale Darstellung mit Null-Auffüllung und x in der Anzeige
System.out.printf( "|%08x| |%#x|\n\n", 0xabc, 0xabc );

double d = 1234.5678;
// Standard-Ausgabe von Dezimalzahlen mit 6 Nachkommastellen
System.out.printf( "|%f| |%f|\n" ,d, -d);
// 2 Nachkommastellen
System.out.printf( "|%.2f| |%.2f|\n" ,d, -d);
// 10 Stellen insgesamt
System.out.printf( "|%10f| |%10f|\n" ,d, -d);
// 10 Stellen gesamt, davon 2 Nachkommastellen
System.out.printf( "|%10.2f| |%10.2f|\n" , d, -d);
// zusätzlich Auffüllen mit Nullen
System.out.printf( "|%010.2f| |%010.2f|\n", d, -d);

*/

		
		int dick = 20;
		String ich = "Tobi";
		System.out.print("Ich bin " + ich + " und habe " + dick + " cm.");
		System.out.println("\nEr sagte: \"No Homo Bruder, aber zeig mal her.\"");
		System.out.println("\nDisclamer: \"Fiktive Geschichte! Leichte Abweichungen sind möglich.\"");
		System.out.print("\n      *\n     ***\n    *****\n   *******\n  *********\n ***********\n*************\n     ***\n     ***\n");

		
		double a= 22.4234234;
		double b= 111.2222;
		double c= 4.0;
		double d= 1000000.551;
		double e= 97.34;
		
		
		System.out.printf( "%.2f\n" ,a);
		System.out.printf( "%.2f\n" ,b);
		System.out.printf( "%.2f\n" ,c);
		System.out.printf( "%.2f\n" ,d);
		System.out.printf( "%.2f\n" ,e);
		
}}
