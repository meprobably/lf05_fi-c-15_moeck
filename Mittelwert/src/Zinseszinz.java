import java.util.Scanner;

public class Zinseszinz {

	public static void main(String[] args) {
		
		  Scanner tastatur = new Scanner(System.in);
		  
	      System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
	      double jahre = tastatur.nextDouble();
	      
	      System.out.print("Wie viel Kapital (in Euro) möchten Sie anlegen: ");
	      double kapital = tastatur.nextDouble();
	      
	      System.out.print("Zinssatz: ");
	      double zinssatz = tastatur.nextDouble();
	      
	      double zinsen;
	      
	      zinssatz = zinssatz * 0.01;
	      
	      int x = 0;
	      
	      while(x < jahre)
	      {
	    	  zinsen = kapital * zinssatz;
	    	  kapital = kapital + zinsen;
	    	  x++;
	    	  
	      }
		System.out.printf("Ihr Kapital beträgt: %.2f€", kapital);
	}
}
