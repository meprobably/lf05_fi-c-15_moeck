import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	  Scanner tastatur = new Scanner(System.in);
      System.out.println("Wie viele Zahlen wollen Sie eingeben?:");
      int anzahl = tastatur.nextInt();
      double summe = 0;
      int zaehler = 0;
      
      while (zaehler < anzahl) {
    	  System.out.println("Geben Sie eine Zahl ein:");
    	  double y = tastatur.nextDouble();
    	  summe = summe + y;
    	  zaehler++;
      }
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      double mittelwert = summe / anzahl;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.print("Der Mittelwert lautet: " + mittelwert);
 
   }
}