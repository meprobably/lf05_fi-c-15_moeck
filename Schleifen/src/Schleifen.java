import java.util.Scanner;

public class Schleifen {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double netto;
		char steuern;
		double zwischenergebnis;
		double endergebnis = 0;
		
		System.out.print("Bitte Nettobetrag in € angeben:");
		netto = tastatur.nextDouble();
		
		System.out.print("Bitte Steuersatz Angeben (j) für ermäßigt und (n) für voll:");
		steuern = tastatur.next().charAt(0);
		
		if(steuern == 'j') {
			
			zwischenergebnis = netto * 0.07;
			endergebnis = netto - zwischenergebnis;
		}
		else if(steuern == 'n') {
			zwischenergebnis = netto * 0.19;
			endergebnis = netto - zwischenergebnis;
		}
		System.out.println("Dein Brutto Betrag ist: " + endergebnis + "€");
	}
}