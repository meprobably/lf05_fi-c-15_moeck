﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
    	Scanner tastatur = new Scanner(System.in);
       
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double restbetrag;
       double ticketpreis = 0;
       int ticketzahl = 0;
       
       double zuZahlenderBetrag;
       
       System.out.print("Ticketpreis (EURO): ");
       ticketpreis = tastatur.nextDouble();
       
       if(ticketpreis < 1) {
      	  ticketpreis = 1;
      	  System.out.println("Tickets gibt es nicht zum 0 Tarif.");
         }

       System.out.print("Wie viele Tickets: ");
       ticketzahl = tastatur.nextInt();
       
       if(ticketzahl > 10) {
    	  ticketzahl = 1;
    	  System.out.println("Mehr als 10 gehen nicht. Die Ausgabe wurde auf 1 gesetzt.");
       }
       else if(ticketzahl < 1){
    	   ticketzahl = 1;
    	   System.out.println("Du willst kein Ticket zum Preis von einem? Du bekommst aber eins. ;)");
       }
       
       
       zuZahlenderBetrag = ticketpreis * ticketzahl;           
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)    	   

      
       {
    	   restbetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
    	   System.out.printf("Noch zu zahlen: ");
    	   System.out.printf("%.2f", (restbetrag));
    	   System.out.printf(" Euro\n");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
    	   
    	   if(eingeworfeneMünze > 2) {
    		   eingeworfeneMünze = 0;
    		   System.out.println("Eine 3 Euro Münze? Bitte Echtgeld einwerfen.");
    	       }
    	   else if(eingeworfeneMünze < 0.05){
    		   eingeworfeneMünze = 0;
    		   System.out.println("Bitte nicht dieses Kleingeld. Mindestens 5 Cent.");
    		   
           }
    	   
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f", (rückgabebetrag));
    	   System.out.printf(" Euro ");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}